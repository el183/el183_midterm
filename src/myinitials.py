from test import MoveGroupTutorial
import argparse
import copy
import geometry_msgs.msg
from moveit_commander.conversions import pose_to_list
import numpy as np

def all_close(goal, actual, tolerance):
  """
  Convenience method for testing if a list of values are within a tolerance of their counterparts in another list
  @param: goal       A list of floats, a Pose or a PoseStamped
  @param: actual     A list of floats, a Pose or a PoseStamped
  @param: tolerance  A float
  @returns: bool
  """
  all_equal = True
  if type(goal) is list:
    for index in range(len(goal)):
      if abs(actual[index] - goal[index]) > tolerance:
        return False

  elif type(goal) is geometry_msgs.msg.PoseStamped:
    return all_close(goal.pose, actual.pose, tolerance)

  elif type(goal) is geometry_msgs.msg.Pose:
    return all_close(pose_to_list(goal), pose_to_list(actual), tolerance)

  return True

class UR5Controller(MoveGroupTutorial):
    def __init__(self):
        super().__init__()
        self.initial_pose = self.group.get_current_pose().pose # define the starting pose

    def go_home(self):
        """
        Move the robot arm to its 'home' position, defined by specific joint angles.

        Returns:
        - bool: True if the final joint values are close to the target values, False otherwise.
        """
         
        group = self.group

        joint_goal = [0]*6
        joint_goal[0] = -np.pi/1.7
        joint_goal[1] = -np.pi/2
        joint_goal[2] = -np.pi/3
        joint_goal[3] = -0.8
        joint_goal[4] = np.pi/2
        joint_goal[5] = np.pi/9

        group.go(joint_goal, wait=True)

        group.stop()

        current_joints = self.group.get_current_joint_values()
        return all_close(joint_goal, current_joints, 0.01)

    def write_and_execute_cartesian_path(self, scale=1):
        """
        Compute and execute a Cartesian path for the robot arm to spell out the letters 'E', 'L' and 'T' in 3D space based on specified waypoints.
        """

        self.go_home() # Ensure the robot is in the home position before starting
        group = self.group                                               

        # Define and execute the path for the letter 'E'
        waypoints_E = self.define_waypoints_E(scale)
        self.execute_waypoints(waypoints_E)
        print("Joint angles after 'E':", group.get_current_joint_values())
        
        # Define and execute the path for the letter 'L'
        waypoints_L = self.define_waypoints_L(scale)
        self.execute_waypoints(waypoints_L)
        print("Joint angles after 'L':", group.get_current_joint_values())

        # Define and execute the path for the letter 'T'
        waypoints_T = self.define_waypoints_T(scale)
        self.execute_waypoints(waypoints_T)
        print("Joint angles after 'T':", group.get_current_joint_values())

    def define_waypoints_E(self, scale):
        """
        Define the waypoints to trace the letter 'E'.
        
        Parameters:
        - scale (float): Scaling factor for the size of the letter.
        
        Returns:
        - list: Waypoints that define the letter 'E'.
        """

        waypoints = []
        wpose = self.group.get_current_pose().pose
        waypoints.append(copy.deepcopy(wpose))

        # Define waypoints to trace the letter 'E'
        wpose.position.x += scale * 0.2  # first right stroke
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z -= scale * 0.15  # go down
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.x -= scale * 0.2  # back to prev
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.x += scale * 0.2  # back to prev
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z -= scale * 0.15  # go down
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.x -= scale * 0.2  # third right stroke
        waypoints.append(copy.deepcopy(wpose))
        return waypoints

    def define_waypoints_L(self, scale):
        """
        Define the waypoints to trace the letter 'L'.
        
        Parameters:
        - scale (float): Scaling factor for the size of the letter.
        
        Returns:
        - list: Waypoints that define the letter 'L'.
        """

        waypoints = []
        wpose = self.group.get_current_pose().pose

        # Return back to top
        wpose.position.z += scale * 0.3
        waypoints.append(copy.deepcopy(wpose))

        # Define waypoints to trace the letter 'L'
        wpose.position.z -= scale * 0.3  # down stroke
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.x -= scale * 0.2  # right stroke
        waypoints.append(copy.deepcopy(wpose))

        return waypoints

    def define_waypoints_T(self, scale):
        """
        Define the waypoints to trace the letter 'T'.
        
        Parameters:
        - scale (float): Scaling factor for the size of the letter.
        
        Returns:
        - list: Waypoints that define the letter 'T'.
        """

        waypoints = []
        wpose = self.group.get_current_pose().pose

        # Return back to top
        wpose.position.z += scale * 0.3
        waypoints.append(copy.deepcopy(wpose))

        # Define waypoints to trace the letter 'T'
        wpose.position.x -= scale * 0.3  # right stroke
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.x += scale * 0.15  # to the middle stroke
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z -= scale * 0.3  # down stroke
        waypoints.append(copy.deepcopy(wpose))
        return waypoints

    def execute_waypoints(self, waypoints):
        """
        Generate and execute a motion plan based on given waypoints.
        
        Parameters:
        - waypoints (list): List of waypoints for the robot to follow.
        """
        (plan, fraction) = self.group.compute_cartesian_path(
                                        waypoints,   # waypoints to follow
                                        0.01,        # eef_step
                                        0.0)         # jump_threshold
        self.execute_plan(plan)

def main():
    controller = UR5Controller()
    controller.write_and_execute_cartesian_path()

if __name__ == "__main__":

    """
        python3 myinitials.py 
    """
    
    main()
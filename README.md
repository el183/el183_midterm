# Midterm - writing initials "EL" in space with end effector

## Installation

``` 
cd el183_midterm & catkin_make
```

## Usage

Terminal 1
```
roslaunch ur5_moveit_config demo.launch limited:=true
```

Terminal 2
```
python3 src/myinitials.py
```

## Demonstration Video

![Initials GIF](el_demo.gif)